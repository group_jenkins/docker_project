package org.sselab.web;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
class Strs {
    @JsonProperty("strList")
    private List<String> strings;
}