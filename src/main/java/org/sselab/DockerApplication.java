package org.sselab;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.*;
import org.sselab.entity.Docker;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@SpringBootApplication
@RestController
public class DockerApplication {
    Logger logger = LoggerFactory.getLogger(DockerApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(DockerApplication.class, args);
    }

    @Value("${server.port}")
    String port;

    @GetMapping("/docker")
    public Docker Docker(@RequestParam String name) {
        return new Docker(name, port);
    }

    @PostMapping("/dockers/{name}/docker/{port}")
    public List<Docker> setDocker(@RequestBody List<Docker> dockers, @PathVariable String name, @PathVariable int port) {
        logger.info(name + "\t" + port);
        return dockers;
    }

    @PutMapping("/dockers/{name}/docker/{port}")
    public void updateDocker(@RequestBody Docker docker, @PathVariable String name, @PathVariable int port) {
        logger.info("put\t" + docker.toString());
        logger.info(name + "\t" + port);
    }

    @DeleteMapping("/docker")
    public void deleteDocker(@RequestParam String name, @RequestParam int port) {
        logger.info(name + "\t" + port);
    }
}

